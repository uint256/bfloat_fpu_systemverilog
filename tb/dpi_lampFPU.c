// Copyright 2019 Politecnico di Milano.
// Copyright and related rights are licensed under the Solderpad Hardware
// Licence, Version 2.0 (the "Licence"); you may not use this file except in
// compliance with the Licence. You may obtain a copy of the Licence at
// https://solderpad.org/licenses/SHL-2.0/. Unless required by applicable law
// or agreed to in writing, software, hardware and materials distributed under
// this Licence is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, either express or implied. See the Licence for the
// specific language governing permissions and limitations under the Licence.
//
// Authors (in alphabetical order):
// Andrea Galimberti    <andrea.galimberti@polimi.it>
// Davide Zoni          <davide.zoni@polimi.it>
//
// Date: 30.09.2019

//
// fadd
//
unsigned int
DPI_fadd(unsigned int op1, unsigned int op2)
{
	//unsigned -> float
	float f_op1 = *((float*) &op1);
	float f_op2 = *((float*) &op2);
	// perform fadd
	float f_res = f_op1 + f_op2;
	// return 32bit float encoding
	return *((unsigned int*) &f_res);
}

//
// fsub
//
unsigned int
DPI_fsub(unsigned int op1, unsigned int op2)
{
	//unsigned -> float
	float f_op1 = *((float*) &op1);
	float f_op2 = *((float*) &op2);
	// perform fsub
	float f_res = f_op1 - f_op2;
	// return 32bit float encoding
	return *((unsigned int*) &f_res);
}

//
// fdiv
//
unsigned int
DPI_fdiv(unsigned int op1, unsigned int op2)
{
	//unsigned -> float
	float f_op1 = *((float*) &op1);
	float f_op2 = *((float*) &op2);
	// perform fdiv
	float f_res = f_op1 / f_op2;
	// return 32bit float encoding
	return *((unsigned int*) &f_res);
}

//
// fmul
//
unsigned int
DPI_fmul(unsigned int op1, unsigned int op2)
{
	//unsigned -> float
	float f_op1 = *((float*) &op1);
	float f_op2 = *((float*) &op2);
	// perform fmul
	float f_res = f_op1 * f_op2;
	// return 32bit float encoding
	return *((unsigned int*) &f_res);
}

//
// feq
//
unsigned int
DPI_feq(unsigned int op1, unsigned int op2)
{
	//unsigned -> float
	float f_op1 = *((float*) &op1);
	float f_op2 = *((float*) &op2);
	// perform feq
	unsigned int res = f_op1 == f_op2;
	// return 1bit comparison flag
	return res;
}

//
// flt
//
unsigned int
DPI_flt(unsigned int op1, unsigned int op2)
{
	//unsigned -> float
	float f_op1 = *((float*) &op1);
	float f_op2 = *((float*) &op2);
	// perform flt
	unsigned int res = f_op1 < f_op2;
	// return 1bit comparison flag
	return res;
}

//
// fle
//
unsigned int
DPI_fle(unsigned int op1, unsigned int op2)
{
	//unsigned -> float
	float f_op1 = *((float*) &op1);
	float f_op2 = *((float*) &op2);
	// perform fle
	unsigned int res = f_op1 <= f_op2;
	// return 1bit comparison flag
	return res;
}

//
// f2i
//
unsigned int
DPI_f2i(unsigned int uVal)
{
	float fVal = *((float*) &uVal);
	int iVal = (int) fVal;
	//printInt(iVal);
	return *((unsigned int*) &iVal);
}

//
// i2f
//
unsigned int
DPI_i2f(int iVal)
{
	float fVal= (float) iVal;
	//printFloatHex(fVal);	
	return *((unsigned int*) &fVal);
}
